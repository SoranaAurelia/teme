/**
Se citesc dintr-un fisier cuvinte unul sub altul
si puncte (nod NULL)
a) sa se creeze arborele cu datele din fisier
b) sa se afiseze preordine inordine postordine
c) sa se afis cuv care
-contin cele mai multe voc,
-iar daca contin ac numar de vocale,
-cel mai lung,
-cel mai mic lexicografic
d) cel mai s=din stanga nod;

        ana
    are         ananas
.   mere      mara         love
    .   .  cheat  life    .    dog
           .  .    .   .      .    cat
                                  .  .

*/

#include <fstream>
#include <cstring>
#include <vector>
#include <algorithm>
using namespace std;

ifstream f("date.in");
ofstream g("date.out");


struct nod{
    char inf[100];
    int nrvocale;
    int length;
    nod *st, *dr;

    nod(char a[100]){
        strcpy(inf, a);
        st = dr = 0;
        nrvocale=0;
        init();
    }

    void init(){
        length=strlen(inf);
        for(int i=0; i<length; i++)
            if(strchr("aeiou", inf[i]))
                nrvocale++;
    }
}*rad, *vmax, *afis[105];

bool cmp(nod* a, nod* b){
    if(a->nrvocale!=b->nrvocale)
        return a->nrvocale>b->nrvocale;
    else if(a->length!=b->length)
        return a->length>b->length;
    if(strcmp(a->inf, b->inf)<0)
        return 1;
    return 0;

}

char a[105];
int viz[105], m;

void citire(nod *&r){
    f.getline(a, 105);
    if(strlen(a) == 0)
        return;
    if(strcmp(a, ".") == 0)
        return;


    r = new nod(a);
    citire(r->st);
    citire(r->dr);
}

void preordine(nod *r, int nivel){
    if(!r)
        return;
    if(cmp(r, vmax) == 1)
        vmax = r;
    if(viz[nivel] == 0){
        afis[nivel] = r;
        viz[nivel] = 1;
        if(nivel>m)
            m=nivel;
    }
    g<<r->inf<<" ";
    preordine(r->st, nivel+1);
    preordine(r->dr, nivel+1);
}

void inordine(nod *r){
    if(!r)
        return;
    inordine(r->st);
    g<<r->inf<<" ";
    inordine(r->dr);
}

void postordine(nod *r){
    if(!r)
        return;

    postordine(r->st);
    postordine(r->dr);
    g<<r->inf<<" ";
}

void afisare(){
    g<<'\n'<<'\n'<<"Lista cuv vele mai din stanga: ";
    for(int i=0; i<=m; i++)
        g<<afis[i]->inf<<", ";

}
int main()
{
    vmax = new nod("");
    vmax->nrvocale=-1;

    citire(rad);
    preordine(rad, 0);
    g<<'\n';
    postordine(rad);
    g<<'\n';
    inordine(rad);
    g<<'\n';

    g<<vmax->inf<<'\n';
    afisare();
//    sort(V.begin(), V.end(), cmp);
//    g<<V.begin()->inf;
    return 0;
}
