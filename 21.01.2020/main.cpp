#include <fstream>
using namespace std;

ifstream f("date.in");
ofstream g("date.out");

class Matrice{
public:
    int n;
    int a[105][105];

    Matrice(){

    }
    Matrice(int n1){
        n = n1;
        for(int i=1; i<=n; i++)
            for(int j=1; j<=n; j++)
                a[i][j] = 0;
    }

    void setZero(int n1){
        n = n1;
        for(int i=1; i<=n; i++)
            for(int j=1; j<=n; j++)
                a[i][j] = 0;
    }
    void setUnit(){
        for(int i=1; i<=n; i++)
            a[i][i]=1;
    }
    Matrice &operator=(const Matrice &other){
        n = other.n;
        for(int i=1; i<=n; i++)
            for(int j=1; j<=n; j++)
                a[i][j] = other.a[i][j];
    }
    Matrice operator*(const Matrice &other){
        Matrice rez(n);

        for(int i=1; i<=n; i++)
            for(int j=1; j<=n; j++)
                for(int k=1; k<=n; k++)
                    rez.a[i][j] = rez.a[i][j] + (a[i][k]*other.a[k][j]);
        return rez;
    }

    Matrice pow(Matrice a, int pow){
        Matrice rez(n);
        rez.setUnit();
        while(pow){
            if(pow%2 == 1)
                rez = rez*a;
            pow/=2;
            a = a*a;
        }
        return rez;
    }

};
int n;
Matrice a;
int l, v1, v2;
void citire(){
    f>>n;
    a.n = n;
    for(int i=1; i<=n; i++){
        for(int j=1; j<=n; j++)
            f>>a.a[i][j];
    }
    f>>l>>v1>>v2;
}

void solve(){
    a = a.pow(a, l);
    int s1=0, s2=0, s3=0;
    for(int i=1; i<=n;i++)
       for(int j=1; j<=n; j++)
        {
            s1 += a.a[i][j];
            if(i == v1)
                s2 +=a.a[i][j];
            if(j == v2)
                s3 += a.a[i][j];
        }
    g<<s1<<'\n'<<s2<<'\n'<<s3;
}
int main()
{

    citire();
    solve();
    return 0;
}
