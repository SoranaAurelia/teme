#include <fstream>
using namespace std;

#define NMAX 150
ifstream f("banda1.in");
ofstream g("banda1.out");

int n, gmax;
int a[NMAX];
int sum[5];

void citire(){
    f>>n>>gmax;
    for(int i=0; i<n; i++)
        f>>a[i];
}
int nr=1, vmin;
int cut1, cut2;

void backt(int k){
    if(k==n){
        if(nr<vmin)
            vmin = nr;
        return;
    }
    for(int v=0; v<2; v++){
        if(sum[v] + a[k]<=gmax){
            sum[v]+=a[k];
            backt(k+1);
            sum[v]-=a[k];
        }
        else{
            int aux = sum[v];
            sum[v]=a[k];
            nr++;
            if(nr<vmin)
                backt(k+1);
            sum[v]=aux;
            nr--;
        }
    }

}

int main()
{
    nr=2;
    citire();
    vmin = n;
    backt(0);
    g<<vmin;
    return 0;
}
