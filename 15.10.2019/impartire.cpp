#include <iostream>
using namespace std;

int n, vmin = 999999999;
int a[20];
int sol[20];
int viz[15];
struct perechi{
    int x, y;
}aa[15];
void citire(){
    cin>>n;
    for(int i=0; i<n; i++)
        cin>>a[i];

}
int suma(){
    int s=0;
    for(int i=0; i<n/2; i++){
        if(a[aa[i].x] > a[aa[i].y])
            s=s+(a[aa[i].x]%a[aa[i].y]);
        else s=s+(a[aa[i].y]%a[aa[i].x]);
    }
    return s;

}
int ok = 1;
void backt(int k){
    if(k==n)
    {
        if(suma() < vmin)
            vmin = suma();
        ok = 0;
        return;
    }
    if(sol[0] == sol[1] && sol[0]==1)
        return;
    for(int i=0; i<(n+1)/2; i++)
        if(viz[i]>0){
            if(viz[i] == 2)
                aa[i].x = k;
            else aa[i].y = k;
            sol[k]=i;
            viz[i]--;
            backt(k+1);
            viz[i]++;
        }
}
int main()
{
    citire();
    for(int i = 0; i<n/2; i++)
        viz[i]=2;
    backt(0);
    cout<<vmin;
    return 0;
}
