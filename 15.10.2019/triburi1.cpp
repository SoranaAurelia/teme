#include <fstream>
#include <algorithm>
using namespace std;
#define MAXN 30010
ifstream f("triburi1.in");
ofstream g("triburi1.out");

int n, k, p; /// k simboluri; a p-a permutare cu rep;
int a[MAXN];
int sol[10];
int poz;
void transfbaza(){

    while(poz && p){
        sol[poz] = p%k;
        p = p /k;
        poz--;
    }
}

int main()
{
    f>>n>>k>>p; /// aflam p-1 in baza k
    poz = k-1;
    p--;
    transfbaza();
    for(int i=1; i<=n; i++)
        f>>a[i];
    sort(a+1, a+n+1);
    for(int i=1; i<=n; i++){
        if(a[i]!=a[i-1]){
            for(int j=0;j<k;j++)
                g<< char(sol[j]+'A'+a[i]-1);
            g<<'\n';
        }
    }
    return 0;
}
