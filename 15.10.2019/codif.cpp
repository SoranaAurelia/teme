#include <fstream>
#include <map>
#include <algorithm>
#include <cstring>
using namespace std;

ifstream f("codif.in");
ofstream g("codif.out");

map<int, char> litere;
int n;
char a[100];
char sol[100];
char solut[100][100];

void valori(){
    litere[1]=' ';
    litere[2]='a';
    litere[4]='e';
    litere[8]='i';
    litere[16]='o';
    litere[32]='u';
    litere[64]='m';
    litere[128]='n';
    litere[256]='r';
    litere[512]='s';
}
int m=0, l;
void backt(int k){
    if(k==n){
        strcpy(solut[m++], sol);
        return;
    }
    ///doar o cif
    int t=a[k]-'0';
    if(litere[t] != NULL){
        sol[l]=litere[t];
        l++;
        backt(k+1);
        l--;
        sol[l]='\0';
    }

    ///doua cif
    if(k+1 > n)
        return;
    t = t*10 + (a[k+1] - '0');
    if(litere[t] != NULL){
        sol[l]=litere[t];
        l++;
        backt(k+2);
        l--;
        sol[l]='\0';
    }

    ///trei cifre
    if(k+2 > n)
        return;
    t = t*10 + (a[k+2] - '0');
    if(litere[t] != NULL){
        sol[l]=litere[t];
        l++;
        backt(k+3);
        l--;
        sol[l]='\0';
    }
}


void sortt(){

    for(int i=0; i<m-1;i++)
        for(int j=i+1; j<m; j++)
            if(strlen(solut[i]) > strlen(solut[j]) ||strlen(solut[i]) == strlen(solut[j]) && strcmp(solut[i], solut[j]) > 0){
                char aux[100]={0};
                strcpy(aux, solut[i]);
                strcpy(solut[i], solut[j]);
                strcpy(solut[j], aux);
            }

}

void afis(){
    g<<m<<'\n';
    for(int i=0; i<m; i++)
        g<<solut[i]<<'\n';
}
int main()
{
    valori();
    f>>n;
    f.get();
    f.getline(a,100);
    backt(0);
    sortt();
    afis();

    return 0;
}
