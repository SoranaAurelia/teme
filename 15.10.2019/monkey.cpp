#include <fstream>
using namespace std;

ifstream f("monkey.in");
ofstream g("monkey.out");

int n, m, viz[36], vmax=-1, lin, col;
char a[35][35];

int dx[]={-1, 0, 1, 0};
int dy[]={0, 1, 0, -1};

void citire(){
    f>>n>>m;
    f.get();
    for(int i=1; i<=n; i++){
        for(int j=1; j<=m; j++){
            f>>a[i][j];
        }
        f.get();
    }
}

void bordare(){

    char x = a[1][1];
    viz[x-'A']=1;
    for(int i = 1; i <= n; i++){
        a[i][0] = x;
        a[i][m+1] = x;
    }
    for(int j = 1; j <= m; j++){
        a[0][j] = x;
        a[n+1][j] = x;
    }

}

void backt(int k){
    if(k>vmax){
        vmax = k;
    }
    for(int v = 0; v<4; v++){
        if(viz[a[lin + dx[v]][col + dy[v]]-'A']==0){
            viz[a[lin + dx[v]][col + dy[v]]-'A'] = 1;
            lin += dx[v];
            col += dy[v];
            backt(k+1);
            viz[a[lin][col]-'A'] = 0;
            lin -= dx[v];
            col -= dy[v];

        }
    }
}
int main()
{
    ios::sync_with_stdio(false);

    citire();
    bordare();
    lin = 1;
    col = 1;
    backt(0);
    g<<vmax+1;

    return 0;
}
